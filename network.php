<?php

if ( ! is_multisite() )
	wp_die( __( 'Multisite support is not enabled.' ) );

if ( ! current_user_can( 'manage_sites' ) )
	wp_die( __( 'You do not have permission to access this page.' ) );

if ( ! empty( $_POST ) && check_admin_referer( basename( __FILE__ ), 'ucc_mfp_network_nonce' ) ) {
	// Require custom Helper class.
	require_once( plugin_dir_path( __FILE__ ) . 'library/class-ucc-mfp-network.php' );
	$helper = new UCC_MFP_Helper;

	// Process actual data here.
	if ( isset( $_POST['ucc_mfp_blogs'] ) ) {
		$_blogs = $_POST['ucc_mfp_blogs'];

		$blogs = get_site_option( 'ucc_mfp_blogs' );
	  	if ( empty( $blogs ) )
			$blogs = array();

		list( $blogs, $added, $subtracted ) = $helper->sync_ids_to_array( $_blogs, $blogs );
		foreach ( $added as $blog ) {
			$msg .= $helper->bulk_add_featured_posts( $blog );
		}
		foreach ( $subtracted as $blog ) {
			$msg .= $helper->bulk_remove_posts( $blog );
		}
		
		update_site_option( 'ucc_mfp_blogs', $blogs );
	}
}

/* Require custom WP_List_Table class. */
require_once( plugin_dir_path( __FILE__ ) . 'library/class-ucc-mfp-network.php' );

$wp_list_table = new UCC_MFP_Network_List_Table();
$pagenum = $wp_list_table->get_pagenum();

$title = __( 'Featured Posts Settings' );

if ( isset( $_REQUEST['ucc_mfp_blogs'] ) ) {
	$msg .= __( 'Settings saved.' );
	if ( $msg )
		$msg = '<div class="updated" id="message"><p>' . $msg . '</p></div>';
}

$wp_list_table->prepare_items();
?>

<div class="wrap">
<?php screen_icon( 'options-general' ); ?>
<h2><?php _e( 'Featured Posts Settings' ) ?>
<?php echo $msg; ?></h2>

<form id="ucc-mfp-network-site-list" action="" method="post">
<?php wp_nonce_field( basename( __FILE__ ), 'ucc_mfp_network_nonce' ); ?>
<?php $wp_list_table->display(); ?>
</form>
</div>
<?php


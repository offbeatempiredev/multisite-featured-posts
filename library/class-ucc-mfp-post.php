<?php
/* Credit: wp-admin/includes/class-wp-posts-list-table.php 
 * Network List Table class: Create a list of sites for enabling/disabling the plugin.
 */

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if ( ! class_exists( 'UCC_MFP_List_Table' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . '/class-ucc-mfp-list-table.php' );
}

class UCC_MFP_Post_List_Table extends UCC_MFP_List_Table {

	function __construct() {
		parent::__construct( array(
			'singular' => 'post',
			'plural' => 'posts',
			'ajax' => false
		) );
	}

	function get_columns() {
		$mfpmode = $this->mfpmode;
	
		switch ( $mfpmode ) {
			case 'select':
				return array(
					'ucc_mfp_select'  => __( 'Feature' ),
					'title'            => __( 'Title' ),
					'author'           => __( 'Author' ),
					'blog_id'          => __( 'Shared by' ),
					'comments'         => '<span class="vers"><img alt="' . esc_attr__( 'Comments' ) . '" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></span>',
					'date'             => __( 'Date' )
				);
			break;
			case 'share':
				return array(
					'ucc_mfp_share'  => __( 'Unshare' ),
					'title'          => __( 'Title' ),
					'author'         => __( 'Author' ),
					'comments'       => '<span class="vers"><img alt="' . esc_attr__( 'Comments' ) . '" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></span>',
					'date'           => __( 'Date' ),
					'ucc_mfp_featured' => __( 'Featured on' )
				);
			break;
			default: // case 'feature'
				return array(
					'ucc_mfp_feature'  => __( 'Unfeature' ),
					'title'            => __( 'Title' ),
					'author'           => __( 'Author' ),
					'blog_id'          => __( 'Shared by' ),
					'comments'         => '<span class="vers"><img alt="' . esc_attr__( 'Comments' ) . '" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></span>',
					'date'             => __( 'Date' )
				);
			
		}
	}

	function get_sortable_columns() {
		return array(
			'title'    => array( 'title', false ),
			'author'   => array( 'author', false ),
			'blog_id'  => array( 'blog', false ),
			'comments' => array( 'comments', false ),
			'date'     => array( 'date', true )
		);
	}

	function prepare_items() {
		global $blog_id, $site_id, $wpdb;
		$blog_id = (int) $blog_id;
		$site_id = (int) $site_id;
		
		$blogs = get_site_option( 'ucc_mfp_blogs' );
		if ( empty( $blogs ) )
			$blogs = array();

		$site_id = (int) $site_id;
		$this->colspan = 6;
		$this->get_request_data();
		$mfpmode = $this->mfpmode;

		// Figure out per page.
		$per_page = $this->get_items_per_page( 'edit_post_per_page' );
		$pagenum = $this->get_pagenum();

		$multisite_posts_table = $wpdb->base_prefix . 'multisite_posts';
		$multisite_postmeta_table = $wpdb->base_prefix . 'multisite_postmeta';
		$multisite_featured_posts_table = $wpdb->base_prefix . 'multisite_featured_posts';
		
		$select_where = "WHERE $multisite_posts_table.blog_id IN (" . implode( ', ', $blogs ) . ")
			AND $multisite_posts_table.site_id = $multisite_postmeta_table.site_id
			AND $multisite_posts_table.blog_id = $multisite_postmeta_table.blog_id
			AND $multisite_posts_table.post_id = $multisite_postmeta_table.post_id
			AND $multisite_postmeta_table.meta_key = 'ucc_mfp_featured_post'
			AND $multisite_posts_table.post_password = ''
			AND $multisite_posts_table.post_type = 'post'
		";
		
		$share_where = "WHERE $multisite_posts_table.blog_id = $blog_id
			AND $multisite_posts_table.site_id = $multisite_postmeta_table.site_id
			AND $multisite_posts_table.blog_id = $multisite_postmeta_table.blog_id
			AND $multisite_posts_table.post_id = $multisite_postmeta_table.post_id
			AND $multisite_postmeta_table.meta_key = 'ucc_mfp_featured_post'
			AND $multisite_posts_table.post_password = ''
			AND $multisite_posts_table.post_type = 'post'	
		";
		
		$feature_where = "WHERE $multisite_featured_posts_table.site_id = $site_id 
			AND $multisite_featured_posts_table.blog_id = $blog_id
			AND $multisite_posts_table.site_id = $multisite_featured_posts_table.featured_site_id
			AND $multisite_posts_table.blog_id = $multisite_featured_posts_table.featured_blog_id
			AND $multisite_posts_table.post_id = $multisite_featured_posts_table.featured_post_id
		";
		
		switch ( $mfpmode ) {
			case 'select':
				// Select Posts to Feature
				$where = $select_where;
			break;
			
			case 'share':
				// Posts I'm Sharing
				$where = $share_where;
			break;
			
			default: // case 'feature'
				// Posts I'm Featuring
				$where = $feature_where;
			break;
		}

		// Set up post counts.
		$this->select_count = $wpdb->get_var( "SELECT COUNT(*) FROM $multisite_posts_table, $multisite_postmeta_table $select_where" );
		$this->share_count = $wpdb->get_var( "SELECT COUNT(*) FROM $multisite_posts_table, $multisite_postmeta_table $share_where" );
		$this->feature_count = $wpdb->get_var( "SELECT COUNT(*) FROM $multisite_posts_table, $multisite_featured_posts_table $feature_where" );


		$orderby = $this->orderby;
		$order = $this->order;
		
		if ( ( '' == $orderby ) && ( '' != $order ) )
			$orderby = 'ORDER BY post_date';
		
		$paged = $this->paged;
		$limit = 'LIMIT ' . intval( ( $pagenum - 1 ) * $per_page ) . ', ' . intval( $per_page );

		if ( $mfpmode == 'feature' ) {
			$from = "FROM $multisite_posts_table, $multisite_featured_posts_table";
		} else {
			$from = "FROM $multisite_posts_table, $multisite_postmeta_table";
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS $multisite_posts_table.* $from $where $orderby $order $limit";
		$result = $wpdb->get_results( $sql, ARRAY_A );
		$this->items = $result;

		$total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );	
		$total_pages = ceil( $total_items / $per_page ); 
		$this->set_pagination_args( array(
			'total_items' => $total_items,
			'total_pages' => $total_pages,
			'per_page' => $per_page
		) );
	}
	
	function no_items() {
		$mfpmode = $this->mfpmode;
		
		switch( $mfpmode ) {
			case 'select':
				_e( 'There are no posts available for featuring. Check your <a href="' . admin_url( 'options-general.php?page=ucc_mfp_admin' ) . '">access settings</a>.' );
				break;
			case 'share':
				_e( 'You have no posts selected for sharing.' );
				break;
			default: // case 'feature'
				_e( 'You are not featuring any posts at this time.' );
		}	
	}

	function get_table_classes() {
		return array( 'widefat', 'fixed', 'posts' );
	}
	
	function extra_tablenav( $which ) {
		global $post_type, $post_type_object, $cat;
		?>
		<div class="alignleft actions">
		</div>
		<?php
	}

	function comments_bubble( $comments ) {
		if ( $comments > 0 )
			echo '<strong>';
		echo "<a class='post-com-count'><span class='comment-count'>" . number_format_i18n( $comments ) . "</span></a>";
		if ( $comments > 0 )
			echo '</strong>';
	}
}

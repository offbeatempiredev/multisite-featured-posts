<?php
/* Credit: wp-admin/includes/class-wp-ms-sites-list-table.php 
 * Generic class for handling plugin data.
 */

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class UCC_MFP_List_Table extends WP_List_Table {

	public $mfpmode = 'featured';

	function __construct() {
		parent::__construct( array(
			'singular' => 'site',
			'plural' => 'sites',
			'ajax' => false
		) );
	}
	
	function display() {
		extract( $this->_args );

		$colspan = $this->colspan;
		$this->display_tablenav( 'top' );

?>
<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>" cellspacing="0">
	<thead>
	<tr>
		<?php $this->print_column_headers(); ?>
	</tr>
	</thead>

	<tfoot>
	<tr>
		<?php $this->print_column_headers( false ); ?>
	</tr>
	</tfoot>

	<tbody id="the-list">
		<?php $this->display_rows_or_placeholder(); ?>
		<?php $this->print_submit_button( $colspan ); ?>
	</tbody>
</table>
<?php
		$this->display_tablenav( 'bottom' );
	}
	
	function has_items() {
		if ( $this->items ) 
			return true;
		else
			return false;
	}

	function pagination( $which ) {
		global $mode;

		parent::pagination( $which );

		if ( 'top' == $which )
			$this->view_switcher( $mode );
	}
	
	function get_column_info() {
		$columns = $this->get_columns(); 
		$hidden = array(); 
		$sortable = $this->get_sortable_columns();

		return array( $columns, $hidden, $sortable );
	}

	function print_column_headers( $with_id = true ) {
		list( $columns, $hidden, $sortable ) = $this->get_column_info();
		$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$current_url = remove_query_arg( 'paged', $current_url );

		if ( isset( $_GET['orderby'] ) )
			$current_orderby = $_GET['orderby'];
		else
			$current_orderby = '';

		if ( isset( $_GET['order'] ) && 'desc' == $_GET['order'] )
			$current_order = 'desc';
		else
			$current_order = 'asc';

		foreach ( $columns as $column_key => $column_display_name ) {
			$class = array( 'manage-column', "column-$column_key" );

			$style = '';
			if ( in_array( $column_key, $hidden ) )
				$style = 'display:none;';

			$style = ' style="' . $style . '"';
			
			switch ( $column_key ) {
				case 'ucc_mfp_network':
					$style = ' style="width:14em;" colspan="2"';
					break;
				case 'lastupdated':
				case 'registered': 
				case 'date':
					$style = ' style="width:10em;"';
					break;
				case 'ucc_mfp_select':
				case 'ucc_mfp_share':
				case 'ucc_mfp_feature':
					$style = ' style="width:6em;"';
					break;
			}		
			
			if ( isset( $sortable[$column_key] ) ) {
				list( $orderby, $desc_first ) = $sortable[$column_key]; 

				if ( $current_orderby == $orderby ) {
					$order = 'asc' == $current_order ? 'desc' : 'asc';
					$class[] = 'sorted';
					$class[] = $current_order;
				} else {
					$order = $desc_first ? 'desc' : 'asc';
					$class[] = 'sortable';
					$class[] = $desc_first ? 'asc' : 'desc';
				}

				$column_display_name = '<a href="' . esc_url( add_query_arg( compact( 'orderby', 'order' ), $current_url ) ) . '"><span>' . $column_display_name . '</span><span class="sorting-indicator"></span></a>';
			}

			$id = $with_id ? "id='$column_key'" : '';

			if ( ! empty( $class ) )
				$class = "class='" . join( ' ', $class ) . "'";
			echo "<th scope='col' $id $class $style>$column_display_name</th>";
		}
	}

	function display_rows() {
		global $current_site, $mode, $blog_id, $site_id, $wpdb;
		
		$blog_id = (int) $blog_id;
		$site_id = (int) $site_id;
		$multisite_featured_posts_table = $wpdb->base_prefix . 'multisite_featured_posts';
				
		list( $columns, $hidden, $sortable ) = $this->get_column_info();
		
		// Get options used in the table.
		$cols = array_keys( $columns );
		if ( in_array( 'ucc_mfp_network', $cols ) ) {
			$blogs = get_site_option( 'ucc_mfp_blogs' );
			if ( empty( $blogs ) )
				$blogs = array();
		}
		
		$rowclass = '';
		foreach ( $this->items as $row ) {
			$rowclass = ( 'alternate' == $rowclass ) ? '' : 'alternate';
			echo "<tr class='$rowclass'>";
			foreach ( $columns as $column_name => $column_display_name ) {
				$class = "$column_name column-$column_name";
				
				$style = '';
				if ( in_array( $column_name, $hidden ) )
					$style = ' style="display:none;"';

				switch ( $column_name ) {
			
					case 'ucc_mfp_network':
						echo "<td valign='top' class='$class'$style><input type='radio' name='ucc_mfp_blogs[" . esc_attr__( $row['blog_id'] ) . "]' value='true'";
						if ( in_array( $row['blog_id'], $blogs ) ) 
							echo " checked='checked'";
						echo " /> Enabled</td>";
						echo "<td valign='top' class='$class'$style><input type='radio' name='ucc_mfp_blogs[" . esc_attr__( $row['blog_id'] ) . "]' value='false'";
						if ( ! in_array( $row['blog_id'], $blogs ) ) 
							echo " checked='checked'";
						echo " /> Disabled</td>";
					break;
					
					case 'ucc_mfp_select': 
						$_blog_id = $row['blog_id'];
						$_post_id = $row['post_id'];
						$sql = "SELECT id FROM $multisite_featured_posts_table WHERE site_id = $site_id 
							AND blog_id = $blog_id 
							AND featured_site_id = $site_id
							AND featured_blog_id = $_blog_id
							AND featured_post_id = $_post_id 
						";
						$result = $wpdb->get_var( $sql );
						if ( $result ) 
							$checked = true;
						else
							$checked = false;
						?>
						<th scope="row" class="check-column">
						<input type="checkbox" name="ucc_mfp_select_posts[<?php esc_attr_e( $row['blog_id'] ); ?>][<?php esc_attr_e( $row['post_id'] ); ?>]" value="checked" <?php checked( $checked ) ?>/>
						<input type="hidden" name="ucc_mfp_shadow_select_posts[<?php esc_attr_e( $row['blog_id'] ); ?>][<?php esc_attr_e( $row['post_id'] ); ?>]" value="checked" />
						</th>
					<?php
					break;
					
					case 'ucc_mfp_share': 
						?>
						<th scope="row" class="check-column">
						<input type="checkbox" name="ucc_mfp_share_posts[<?php esc_attr_e( $row['blog_id'] ); ?>][<?php esc_attr_e( $row['post_id'] ); ?>]" value="checked" />
						<input type="hidden" name="ucc_mfp_shadow_share_posts[<?php esc_attr_e( $row['blog_id'] ); ?>][<?php esc_attr_e( $row['post_id'] ); ?>]" value="checked" />
						</th>
					<?php
					break;

					case 'ucc_mfp_feature': 
						?>
						<th scope="row" class="check-column">
						<input type="checkbox" name="ucc_mfp_feature_posts[<?php esc_attr_e( $row['blog_id'] ); ?>][<?php esc_attr_e( $row['post_id'] ); ?>]" value="checked" />
						<input type="hidden" name="ucc_mfp_shadow_feature_posts[<?php esc_attr_e( $row['blog_id'] ); ?>][<?php esc_attr_e( $row['post_id'] ); ?>]" value="checked" />
						</th>
					<?php
					break;
					
					case 'ucc_mfp_featured':
						$_blog_id = $row['blog_id'];
						$_post_id = $row['post_id'];
						$sql = "SELECT blog_id FROM $multisite_featured_posts_table 
							WHERE featured_site_id = $site_id
								AND featured_blog_id = $_blog_id
								AND featured_post_id = $_post_id 
						";
						$shared_by_blogs = $wpdb->get_results( $sql, ARRAY_A );
						echo "<td valign='top' class='$class'$style>";
						if ( $shared_by_blogs ) {
							foreach ( $shared_by_blogs as $shared_by_blog ) {
								$details = get_blog_details( $shared_by_blog['blog_id'] );
								echo '<a href="' . $details->siteurl . '">' . $details->blogname . '</a><br />';
							}
						}
						echo "</td>";
					break;
					
					case 'id':
					?>
						<th valign="top" scope="row">
							<?php echo $row['blog_id'] ?>
						</th>
					<?php
					break;

					case 'blogname':
						$blogname =( is_subdomain_install() ) ? str_replace( '.' . $current_site->domain, '', $row['domain'] ) : $row['path'] ;
						
						echo "<td valign='top' class='$class'$style>";
						?>
							<a href="<?php echo esc_url( network_admin_url( 'site-info.php?id=' . $row['blog_id'] ) ); ?>" class="edit"><?php echo $blogname . $blog_state; ?></a>
							<?php
							if ( 'list' != $mode )
								echo '<p>' . sprintf( _x( '%1$s &#8211; <em>%2$s</em>', '%1$s: site name. %2$s: site tagline.' ), get_blog_option( $row['blog_id'], 'blogname' ), get_blog_option( $row['blog_id'], 'blogdescription ' ) ) . '</p>';
					?>
						</td>
					<?php
					break;

					case 'lastupdated':
						echo "<td valign='top' class='$class'$style>";
							if ( 'list' == $mode )
								$date = 'Y/m/d';
							else
								$date = 'Y/m/d \<\b\r \/\> g:i:s a';
							echo ( $row['last_updated'] == '0000-00-00 00:00:00' ) ? __( 'Never' ) : mysql2date( $date, $row['last_updated'] ); ?>
						</td>
					<?php
					break;

					case 'registered':
						echo "<td valign='top' class='$class'$style>";
						if ( $row['registered'] == '0000-00-00 00:00:00' )
							echo '&#x2014;';
						else
							echo mysql2date( $date, $row['registered'] );
						?>
						</td>
					<?php
					break;
					
					case 'date':
						echo "<td valign='top' class='$class'$style>";
						if ( '0000-00-00 00:00:00' == $row['post_date'] ) {
							_e( 'Unpublished' );
						} else {
							if ( 'list' == $mode )
								$date = 'Y/m/d';
							else
								$date = 'Y/m/d \<\b\r \/\> g:i:s a';
							echo ( $row['post_date'] == '0000-00-00 00:00:00' ) ? __( 'Never' ) : mysql2date( $date, $row['post_date'] );
						}
						echo '<br />';
						switch ( $row['post_status'] ) {
							case 'publish':
								_e( 'Published' );
							break;
							
							case 'future':
								_e( 'Scheduled' );
							break;
							
							default:
								_e( 'Last Modified' );
						}
						echo '</td>';
					break;
		
					case 'title':
						$attributes = 'class="post-title page-title column-title"' . $style;
						$title = esc_attr__( $row['post_title'] );
						if ( empty( $title ) )
							$title = '(no title)';
						echo "<td valign='top' class='$class'$style>";
						?>
						<strong><a class="row-title" href="<?php _e( esc_url( $row['permalink'] ) ) ?>" title="<?php _e( $title ) ?>"><?php _e( $title ) ?></a></strong>
						<?php
						if ( 'excerpt' == $mode ) {
							$excerpt = esc_html__( apply_filters( 'get_the_excerpt', $post['post_excerpt'] ) );
							if ( empty( $excerpt ) ) {
								// Make an excerpt if one does not exist.
								$excerpt = substr( strip_tags( $row['post_content'] ), 0, 200 );
								$excerpt = esc_html__( apply_filters( 'get_the_excerpt', $excerpt . '&hellip;' ) );
							}
							echo '<p>' . $excerpt . '</p>';
						}	
						echo '</td>';
					break;
		
					case 'comments':
						echo "<td valign='top' class='$class'$style>";
						?>
						<div class="post-com-count-wrapper">
						<?php
						$this->comments_bubble( $row['comment_count'] );
						?>
						</div></td>
						<?php
					break;
		
					case 'author':
					?>
					<td valign='top' class='<?php _e( $class ) ?>'<?php _e( $style ) ?>><?php
						esc_html_e( get_the_author_meta( 'user_nicename', $row['post_author'] ) );
					?></td>
					<?php
					break;
		
					case 'blog_id':
					?>
					<td valign='top' class='<?php _e( $class ) ?>'<?php _e( $style ) ?>><a href="<?php _e( esc_url( get_blog_option( $row['blog_id'], 'siteurl') ) ) ?>"><?php esc_html_e( get_blog_option( $row['blog_id'], 'blogname' ) ); ?></a></td>
					<?php
					break;

					default:
						echo "<td class='$class'$style>";
						echo "</td>";
					break;
				}
			}
			?>
			</tr>
			<?php
		}
	}
	
	function print_submit_button( $colspan ) {
	?>
		<td colspan="<?php _e( $colspan ); ?>">
<?php submit_button( __( 'Save Settings' ), 'primary', false, false, array( 'id' => 'ucc-mfp-submit' ) ); ?>
		</td>
	<?php
	}

	function get_request_data() {
		global $mode;

		$mode = ( isset( $_REQUEST['mode'] ) && ( 'excerpt' == $_REQUEST['mode'] ) ? 'excerpt' : 'list' );
		$this->mode = $mode;

		$orderby = isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : '';
		switch ( $orderby ) {
			case 'registered':
				$orderby = 'ORDER BY registered';
				break;
			case 'lastupdated':
				$orderby = 'ORDER BY last_updated';
				break;
			case 'blogname':
				if ( is_subdomain_install() )
					$orderby = 'ORDER BY domain';
				else
					$orderby = 'ORDER BY path';
				break;
			case 'author':
				$orderby = 'ORDER BY post_author';
				break;
			case 'blog':
				$orderby = 'ORDER BY blog_id';
				break;
			case 'comments':
				$orderby = 'ORDER BY comment_count';
				break;
			case 'date':
				$orderby = 'ORDER BY post_date';
				break;
			case 'title':
				$orderby = 'ORDER BY post_title';
				break;
			default:
				$orderby = '';
		}
		$this->orderby = $orderby;

		$order = ( isset( $_REQUEST['order'] ) && ( 'desc' == $_REQUEST['order'] ) ? "DESC" : "ASC" );
		$this->order = $order;
		
		$mfpmode = isset( $_REQUEST['mfpmode'] ) ? $_REQUEST['mfpmode'] : '';
		switch ( $mfpmode ) {
			case 'select':
				$mfpmode = 'select';
				break;
			case 'share':
				$mfpmode = 'share';
				break;	
			default:
				$mfpmode = 'feature';
		}
		$this->mfpmode = $mfpmode;
	}
}

?>

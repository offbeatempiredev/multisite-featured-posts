<?php

class UCC_MFP_Helper {

	// Adds/subtracts arr1 keys from arr2 based on value.
	// Returns arrays of synced arr2, added keys, subtracted keys.
	function sync_ids_to_array( $arr1, $arr2 ) {
		$added = array();
		$subtracted = array();
		foreach ( $arr1 as $key => $value ) {
			$key = (int) $key;
			if ( $value == 'true' ) {
				if ( ! in_array( $key, $arr2 ) ) {
					$arr2[] = $key;	
					$added[] = $key;
				}
			} else {
				if ( in_array( $key, $arr2 ) ) {
					$k = array_search( $key, $arr2 );
					unset( $arr2[$k] );
					$subtracted[] = $key;
				}
			}
		}
		$result = array( $arr2, $added, $subtracted );

		return $result;
	}

	function get_blog_site( $blog ) {
		global $wpdb;
	
		$blog = (int) $blog;
		$blogs_table = $wpdb->base_prefix . 'blogs';
		$site = $wpdb->get_var( $wpdb->prepare( "SELECT site_id FROM $blogs_table WHERE blog_id = $blog" ) );
		
		return $site;
	}

	function bulk_add_featured_posts( $blog ) {
		global $wpdb;

		$blog = (int) $blog;

		$table_prefix = $wpdb->base_prefix . ( $blog == 1 ? '' : $blog . '_' ) ;
		$sql = "SELECT " . $table_prefix . "posts.* 
			FROM " . $table_prefix . "posts 
				INNER JOIN " . $table_prefix . "postmeta ON ( 
					" . $table_prefix . "posts.ID = " . $table_prefix . "postmeta.post_id 
				) 
			WHERE " . $table_prefix . "posts.post_type = 'post' 
			AND ( " . $table_prefix . "posts.post_status = 'publish') 
			AND ( " . $table_prefix . "postmeta.meta_key = 'ucc_mfp_featured_post' ) 
			GROUP BY " . $table_prefix . "posts.ID ORDER BY " . $table_prefix . "posts.post_date DESC LIMIT 0, " . UCC_MFP_BULK_LIMIT;
		$result = $wpdb->get_results( $sql, OBJECT_K );
		$msg .= "Checking blog id $blog for featured posts…";

		if ( $result ) {
			$msg .= ' Posts found.<br />';
			$site = $this->get_blog_site( $blog );
			
			foreach ( (array) $result as $row ) {
				$post = $row->ID;
				$postarr = $row;
				// INSERT takes priority because we're adding a new blog.
				$result = $this->insert_multisite_post( $site, $blog, $post, $postarr );
				if ( ! $result ) {
					$result = $this->update_multisite_post( $site, $blog, $post, $postarr );
					$msg .= 'Updating post ' . $postarr->ID . ': <strong>' . esc_html( $postarr->post_title ) . '</strong><br />';
				} else {
					$msg .= 'Inserting post ' . $postarr->ID . ': <strong>' . esc_html( $postarr->post_title ) . '</strong><br />';
				}
			}
			return $msg;
		} else {
			$msg .= ' No posts found.<br />';
			return $msg;
		}
	}
	
	function bulk_remove_posts( $blog ) {
		global $wpdb;
		
		$site = $this->get_blog_site( $blog );
		
		$multisite_posts_table = $wpdb->base_prefix . 'multisite_posts';
		$multisite_postmeta_table = $wpdb->base_prefix . 'multisite_postmeta';
		$sql = "DELETE FROM $multisite_posts_table WHERE blog_id = $blog AND site_id = $site";
		$result = $wpdb->query( $sql );
		$sql = "DELETE FROM $multisite_postmeta_table WHERE blog_id = $blog AND site_id = $site";
		$result = $wpdb->query( $sql );
		$msg .= "Deleting blog id $blog posts and postmeta from plugin tables.<br />";
		
		return $msg;
	}

	function insert_multisite_post( $site, $blog, $post, $postarr ) {
		global $wpdb;

		$permalink = $this->get_permalink( $site, $blog, $post );
		$multisite_posts_table = $wpdb->base_prefix . 'multisite_posts';
		$data = array(
			'site_id'	       => $site,
			'blog_id'	       => $blog,
			'post_id'	       => $post,
			'permalink'	     => $permalink,
			'post_author'	   => $postarr->post_author,
			'post_date'		=> $postarr->post_date,
			'post_date_gmt'	 => $postarr->post_date_gmt,
			'post_content'	  => $postarr->post_content,
			'post_title'	    => $postarr->post_title,
			'post_excerpt'	  => $postarr->post_excerpt,
			'post_status'	   => $postarr->post_status,
			'comment_status'	=> $postarr->comment_status,
			'ping_status'	   => $postarr->ping_status,
			'post_password'	 => $postarr->post_password,
			'post_name'		=> $postarr->post_name,
			'to_ping'	       => $postarr->to_ping,
			'pinged'		=> $postarr->pinged,
			'post_modified'	 => $postarr->post_modified,
			'post_modified_gmt'     => $postarr->post_modified_gmt,
			'post_content_filtered' => $postarr->post_content_filtered,
			'post_parent'	   => $postarr->post_parent,
			'guid'		  => $postarr->guid,
			'menu_order'	    => $postarr->menu_order,
			'post_type'		=> $postarr->post_type,
			'post_mime_type'	=> $postarr->post_mime_type,
			'comment_count'	 => $postarr->comment_count
		);
		$data_format = array( '%d', '%d', '%d', '%s', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%d', '%s', '%s', '%d' );
		$result = $wpdb->insert( $multisite_posts_table, $data, $data_format );

		if ( $result ) {
			$postmeta = $this->insert_or_update_multisite_postmeta( $site, $blog, $post );	
		}

		return $result;
	}

	function update_multisite_post( $site, $blog, $post, $postarr ) {
		global $wpdb;

		$permalink = $this->get_permalink( $site, $blog, $post );
		$multisite_posts_table = $wpdb->base_prefix . 'multisite_posts';
		$data = array(
			'permalink'             => $permalink,
			'post_author'           => $postarr->post_author,
			'post_date'             => $postarr->post_date,
			'post_date_gmt'         => $postarr->post_date_gmt,
			'post_content'          => $postarr->post_content,
			'post_title'            => $postarr->post_title,
			'post_excerpt'          => $postarr->post_excerpt,
			'post_status'           => $postarr->post_status,
			'comment_status'        => $postarr->comment_status,
			'ping_status'           => $postarr->ping_status,
			'post_password'         => $postarr->post_password,
			'post_name'             => $postarr->post_name,
			'to_ping'               => $postarr->to_ping,
			'pinged'                => $postarr->pinged,
			'post_modified'         => $postarr->post_modified,
			'post_modified_gmt'     => $postarr->post_modified_gmt,
			'post_content_filtered' => $postarr->post_content_filtered,
			'post_parent'           => $postarr->post_parent,
			'guid'                  => $postarr->guid,
			'menu_order'            => $postarr->menu_order,
			'post_type'             => $postarr->post_type,
			'post_mime_type'        => $postarr->post_mime_type,
			'comment_count'         => $postarr->comment_count
		);
		$data_format = array( '%s', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%d', '%s', '%s', '%d' );
		$where = array(
			'site_id' => $site,
			'blog_id' => $blog,
			'post_id' => $post
		);
		$where_format = array( '%d', '%d', '%d' );
		$result = $wpdb->update( $multisite_posts_table, $data, $where, $data_format, $where_format );

		if ( $result ) {
			$postmeta = $this->insert_or_update_multisite_postmeta( $site, $blog, $post );
		}

		return $result;
	}

	function delete_multisite_post( $site, $blog, $post ) {
		global $wpdb;

		$multisite_posts_table = $wpdb->base_prefix . 'multisite_posts';
		$result = $wpdb->query( $wpdb->prepare( "DELETE FROM $multisite_posts_table WHERE site_id = %d AND blog_id = %d AND post_id = %d", $site, $blog, $post ) );

		if ( $result ) {
			$postmeta = $this->delete_multisite_postmeta( $site, $blog, $post );
		}

		return $result;
	}

	function insert_or_update_multisite_postmeta( $site, $blog, $post ) {
		global $wpdb;

		$postmeta_table = $wpdb->base_prefix . ( $blog == 1 ? '' : $blog . '_' ) . 'postmeta';
		$multisite_postmeta_table = $wpdb->base_prefix . 'multisite_postmeta';
		$result = $wpdb->query( $wpdb->prepare( "DELETE FROM $multisite_postmeta_table WHERE site_id = %d AND blog_id = %d AND post_id = %d", $site, $blog, $post ) );
		$result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $postmeta_table WHERE post_id = %d", $post ), OBJECT_K );

		foreach ( (array) $result as $row ) {
			$data = array(
				'site_id'    => $site,
				'blog_id'    => $blog, 
				'meta_id'    => $row->meta_id,
				'post_id'    => $post,
				'meta_key'   => $row->meta_key,
				'meta_value' => $row->meta_value
			);
			$data_format = array( '%d', '%d', '%d', '%d', '%s', '%s' );
			$wpdb->insert( $multisite_postmeta_table, $data, $data_format );
		}
	}

	function delete_multisite_postmeta( $site, $blog, $post ) {
		global $wpdb;

		$multisite_postmeta_table = $wpdb->base_prefix . 'multisite_postmeta';
		$result = $wpdb->query( $wpdb->prepare( "DELETE FROM $multisite_postmeta_table WHERE site_id = %d AND blog_id = %d AND post_id = %d", $site, $blog, $post ) );

		return $result;
	}

	function is_featured_post( $blog, $post ) {
		global $wpdb;

		$postmeta_table = $wpdb->base_prefix . ( $blog == 1 ? '' : $blog . '_' ) . 'postmeta';
		$result = $wpdb->get_var( $wpdb->prepare( "SELECT meta_id FROM $postmeta_table WHERE post_id = %d AND meta_key = 'ucc_mfp_featured_post'", $post ) );

		return $result;
	}

	function get_permalink( $site, $blog, $post ) {
		global $wpdb;

		$postmeta_table = $wpdb->base_prefix . ( $blog == 1 ? '' : $blog . '_' ) . 'postmeta';
		$result = $wpdb->get_var( $wpdb->prepare( "SELECT meta_value FROM $postmeta_table WHERE post_id = %d AND meta_key = 'ucc_mfp_featured_post'", $post ) );

		return $result;
	}
}

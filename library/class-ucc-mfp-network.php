<?php
/* Credit: wp-admin/includes/class-wp-ms-sites-list-table.php 
 * Network List Table class: Create a list of sites for enabling/disabling the plugin.
 */

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if ( ! class_exists( 'UCC_MFP_List_Table' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . '/class-ucc-mfp-list-table.php' );
}

class UCC_MFP_Network_List_Table extends UCC_MFP_List_Table {

	function __construct() {
		parent::__construct( array(
			'singular' => 'site',
			'plural' => 'sites',
			'ajax' => false
		) );
	}

	function get_columns() {
		$blogname_columns = ( is_subdomain_install() ) ? __( 'Domain' ) : __( 'Path' );
		$columns = array(
			'ucc_mfp_network' => __( 'Multisite Featured Posts' ),
			'blogname'    => $blogname_columns,
			'lastupdated' => __( 'Last Updated' ),
			'registered'  => _x( 'Registered', 'site' )
		);

		return $columns;
	}

	function get_sortable_columns() {
		return array(
			'blogname'    => array( 'blogname', false ),
			'lastupdated' => array( 'lastupdated', false ),
			'registered'  => array( 'blog_id', true )
		);
	}
	
	function prepare_items() {
		global $mode, $wpdb, $current_site;

		$this->colspan = 5;
		$this->get_request_data();

		// Figure out per page.
		$per_page = $this->get_items_per_page( 'sites_network_per_page' );
		$pagenum = $this->get_pagenum();

		$large_network = false;
		// If the network is large and a search is not being performed, show only the latest blogs with no paging in order
		// to avoid expensive count queries.
		if ( ( get_blog_count() >= 10000 ) ) {
			if ( ! isset($_REQUEST['orderby']) )
				$this->orderby = '';
			if ( ! isset($_REQUEST['order']) )
				$this->order = 'DESC';
			$large_network = true;
		}
		
		$blogs_table = $wpdb->base_prefix . 'blogs';
		$where = "WHERE site_id = $current_site->id AND public = '1' AND archived = '0' AND mature = '0' AND spam = '0' AND deleted = '0'";
		$orderby = $this->orderby;
		$order = $this->order;
		
		if ( ( '' == $orderby ) && ( '' != $order ) )
			$orderby = 'ORDER BY blog_id';
		
		$paged = $this->paged;
		$limit = 'LIMIT ' . intval( ( $pagenum - 1 ) * $per_page ) . ', ' . intval( $per_page );

		$sql = "SELECT * FROM $blogs_table $where $orderby $order $limit";
		$result = $wpdb->get_results( $sql, ARRAY_A );
		$this->items = $result;

        // Don't do an unbounded count on large networks
        if ( ! $large_network )
			$total = $wpdb->get_var( 'SELECT COUNT( blog_id ) FROM ' . $blogs_table );
		else
			$total = count( $this->items );

		$this->set_pagination_args( array(
			'total_items' => $total,
			'per_page' => $per_page,
		) );
	}
	
	function no_items() {
		_e( 'No sites found.' );
	}	
}
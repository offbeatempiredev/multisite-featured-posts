<?php
/*
Plugin Name: Multisite Featured Posts 
Plugin URI: http://uncommoncontent.com/wordpress/plugins/multisite-featured-posts
Description:
Network: true
Version: 0.1
Author: Jennifer M. Dodd
Author URI: http://bajada.net
*/ 

/*
	Copyright 2011 Jennifer M. Dodd (email: jmdodd@gmail.com)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

define( 'UCC_MFP_BULK_LIMIT', 20 );

// Load UCC_MFP_Helper class.
include_once( plugin_dir_path( __FILE__ ) . 'library/class-ucc-mfp-helper.php' );

// Plugin activation and deactivation hooks.
include_once(  plugin_dir_path( __FILE__ ) . 'upgrade.php' );
register_activation_hook(  plugin_dir_path( __FILE__ ) . 'multisite-featured-posts.php', 'ucc_mfp_activation' );
register_deactivation_hook(  plugin_dir_path( __FILE__ ) . 'multisite-featured-posts.php', 'ucc_mfp_deactivation' );

// Register query var.
function ucc_mfp_add_query_var() {
	global $wp;
	$wp->add_query_var( 'featured' );
}
add_action( 'init', 'ucc_mfp_add_query_var' );

// Load language settings: next pass.
//function ucc_mfp_text_domain() {
//	load_plugin_textdomain( 'multisite-featured-posts', basename( dirname( __FILE__ ) )  . 'languages', 'multisite-featured-posts/languages' );
//}
//add_action( 'init', 'ucc_mfp_text_domain' );

// Create a Network Admin page for enabling/disabling the plugin on a per-site basis.
function ucc_mfp_network() {
	require_once( plugin_dir_path( __FILE__ ) . 'network.php' );
}

function ucc_mfp_network_admin_menu() {
	// Ensure plugin.php is loaded.
	if ( ! function_exists( 'is_plugin_active_for_network' ) )
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

	// Make sure plugin is Network Activated.
	if ( is_plugin_active_for_network( 'multisite-featured-posts/multisite-featured-posts.php' ) ) {
		add_submenu_page( 'settings.php', 'Multisite Featured Posts', 'Featured Posts', 'manage_options', 'ucc_mfp_network', 'ucc_mfp_network' );
	}
}
add_action( 'network_admin_menu', 'ucc_mfp_network_admin_menu' );


// Create an Admin page for managing featured posts.
function ucc_mfp_post() {
	require_once( plugin_dir_path( __FILE__ ) . 'post.php' );
}

// Add Admin pages to menu for allowed users. 
function ucc_mfp_admin_menu() {
	global $blog_id;

	if ( ! function_exists( 'is_plugin_active_for_network' ) )
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

	if ( is_plugin_active_for_network( 'multisite-featured-posts/multisite-featured-posts.php' ) ) {
		if ( current_user_can( 'manage_options' ) ) {
			$allowed_blogs = get_site_option( 'ucc_mfp_blogs', false );
			if ( empty( $allowed_blogs ) )
				$allowed_blogs = array();
			if ( $allowed_blogs ) {
				if ( in_array( $blog_id, $allowed_blogs ) ) {
					add_submenu_page( 'edit.php', 'Multisite Featured Posts', 'Featured Posts', 'manage_options', 'ucc_mfp_post', 'ucc_mfp_post' );
				}
			}
		}
	}
}
add_action( 'admin_menu', 'ucc_mfp_admin_menu' );

function ucc_mfp_admin_init() {
	global $blog_id;

	if ( ! function_exists( 'is_plugin_active_for_network' ) )
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

	if ( is_plugin_active_for_network( 'multisite-featured-posts/multisite-featured-posts.php' ) ) {
		if ( current_user_can( 'edit_posts' ) ) {
			$allowed_blogs = get_site_option( 'ucc_mfp_blogs', false );
			if ( empty( $allowed_blogs ) )
				$allowed_blogs = array();
			if ( $allowed_blogs ) {
				if ( in_array( $blog_id, $allowed_blogs ) ) {	
					// Add column to Posts screen.
					add_filter( 'manage_post_posts_columns', 'ucc_mfp_manage_posts_columns' );
					add_action( 'manage_posts_custom_column', 'ucc_mfp_manage_custom_column', 10, 2 );
					add_action( 'add_meta_boxes', 'ucc_mfp_add_meta_box' );
					add_filter( 'manage_edit-post_sortable_columns', 'ucc_mfp_column_register_sortable', 11 );
					add_filter( 'request', 'ucc_mfp_column_orderby', 11 );
					add_filter( 'parse_query', 'ucc_mfp_parse_query' );

					// Add checkbox to Quick Edit.
					add_action( 'quick_edit_custom_box',  'ucc_mfp_quick_edit_custom_box', 10, 2 );
					add_action( 'admin_head-edit.php', 'ucc_mfp_quick_edit_javascript' );
					add_action( 'wp_ajax_ucc_mfp_get', 'ucc_mfp_get_callback' );
					
					// Add meta box to Add New/Edit Post screen.
					add_action( 'add_meta_boxes', 'ucc_mfp_add_meta_box' );
				}
			}
		}
	}
}
add_action( 'admin_init', 'ucc_mfp_admin_init' );


// Add column to Posts screen.
function ucc_mfp_manage_posts_columns( $columns ) {
	$columns['featured'] = '&hearts;';
	return $columns;
}

function ucc_mfp_manage_custom_column( $column_name, $id ) {
	if ( $column_name != 'featured')
		return;

	$featured = get_post_meta( $id, 'ucc_mfp_featured_post' );
	if ( $featured )
		echo '<strong>&hearts;</strong>';		
}

function ucc_mfp_column_register_sortable( $columns ) {
	$columns['featured'] = 'featured';
 
	return $columns;
}

function ucc_mfp_column_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'featured' == $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'meta_key' => 'ucc_mfp_featured_post'
		) );
	}
 
	return $vars;
}

function ucc_mfp_parse_query( $query ) {
	global $pagenow;

	if ( is_admin() && ( $pagenow == 'edit.php' ) &&
		isset( $_GET['featured'] ) && ( (int) $_GET['featured'] == 1 ) )  {
		$query->set( 'meta_query', array(
			array(
				'key' => 'ucc_mfp_featured_post'
			)
		) );
	}
	return $query;
}


// Add checkbox to Quick Edit.
function ucc_mfp_quick_edit_custom_box( $column_name, $post_type ) {
	if ( $column_name != 'featured')
		return;
	
	if ( $post_type != 'post' )
		return;
	?>
	<fieldset class="inline-edit-col-right"><div class="inline-edit-col">
	<?php wp_nonce_field( plugin_basename( __FILE__ ), 'ucc_mfp_nonce' ); ?>
	<input type="hidden" name="is_quickedit" value="true" />
	<div class="inline-edit-group">
		<label class="alignleft">
			<input type="checkbox" name="ucc_mfp_featured_post" id="ucc_mfp_featured_post_check" value="true" />
			<span class="checkbox-title">Allow to be featured (including on other blogs)</span>
		</label>
	</div></div></fieldset>
	<?php
}

function ucc_mfp_quick_edit_javascript() {
	global $current_screen;
	
	if ( ( $current_screen->id != 'edit-post' ) || ( $current_screen->post_type != 'post' ) )
		return; 
		
	$ajax_nonce = wp_create_nonce( 'ucc_mfp_ajax_nonce' );
 	?>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery("a.editinline").live("click", function() {
		var post_id = inlineEditPost.getId(this);
		jQuery.post(
			ajaxurl,
			{action:'ucc_mfp_get', security:'<?php echo $ajax_nonce; ?>', ucc_post_id:post_id},
			function(data) {
				if (data == 'true')
					jQuery('#ucc_mfp_featured_post_check').attr('checked', true);
				else
					jQuery('#ucc_mfp_featured_post_check').attr('checked', false);
			}
		);
	});
});
</script>
	<?php
}

function ucc_mfp_get_callback() {
	global $wpdb;

	check_ajax_referer( 'ucc_mfp_ajax_nonce', 'security' );
	$post = intval( $_REQUEST['ucc_post_id'] );
	$checked = get_post_meta( $post, 'ucc_mfp_featured_post', true );
	if ( $checked )
		echo 'true';
	else
		echo 'false';
	die();
}


//	Display the Featured Post meta box on the Add New/Edit Post screen.	
function ucc_mfp_meta_box() {
	global $post;

	wp_nonce_field( plugin_basename( __FILE__ ), 'ucc_mfp_nonce' );
	$current = get_post_meta( $post->ID, 'ucc_mfp_featured_post', true );

	echo '<table class="form-table"><tr><th><label for="ucc_mfp_featured_post">Featured Post</label></th><td><input type="checkbox" name="ucc_mfp_featured_post" value="true"';
	if ( $current ) 
		echo ' checked="checked"';
	echo ' /></td></tr></table>';
}

function ucc_mfp_add_meta_box() {
	global $blog_id;

	// Ensure plugin.php is loaded.
	if ( ! function_exists( 'is_plugin_active_for_network' ) )
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

	// Make sure plugin is Network Activated.
	if ( is_plugin_active_for_network( 'multisite-featured-posts/multisite-featured-posts.php' ) ) {
		$allowed_blogs = get_site_option( 'ucc_mfp_blogs', false );
		if ( empty( $allowed_blogs ) )
			$allowed_blogs = array();

		if ( $allowed_blogs ) {
			if ( in_array( $blog_id, $allowed_blogs ) )
				add_meta_box( 'ucc-mfp-meta-box', 'Featured Post', 'ucc_mfp_meta_box', 'post', 'side', 'default' );
		}			
	}
}


// All this does is deal with the post_meta.
function ucc_mfp_save_post( $post ) {
	global $blog_id;

	// For lack of a better term, something has to stick.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	if ( defined( 'DOING_CRON' ) && DOING_CRON ) 
		return;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		if ( $_POST['is_quickedit'] != 'true' ) 
			return;
	}

	if ( ! wp_verify_nonce( $_POST['ucc_mfp_nonce'], plugin_basename( __FILE__ ) ) )
		return;

	if ( ! current_user_can( 'edit_post', $post ) )
		return;

	if ( wp_is_post_revision( $post ) || wp_is_post_autosave( $post ) ) 
		return;
		
	if ( isset( $_POST['ucc_mfp_featured_post'] ) && ( $_POST['ucc_mfp_featured_post'] == 'true' ) ) {
		$permalink = get_permalink( $post );
		update_post_meta( $post, 'ucc_mfp_featured_post', $permalink );
	} else {
		delete_post_meta( $post, 'ucc_mfp_featured_post' );
	}
}
add_action( 'save_post', 'ucc_mfp_save_post' );

// Filter on wp_insert_post checks for enabled blog, then copies relevant fields to database.
function ucc_mfp_wp_insert_post( $post, $postarr ) {
	global $blog_id, $site_id, $wpdb;

	// For lack of a better term, something has to stick.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	if ( defined( 'DOING_CRON' ) && DOING_CRON ) 
		return;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		if ( $_POST['is_quickedit'] != 'true' ) 
			return;
	}
	
	if ( ! wp_verify_nonce( $_POST['ucc_mfp_nonce'], plugin_basename( __FILE__ ) ) )
		return;
	
	if ( ! current_user_can( 'edit_post', $post ) )
		return;

	if ( wp_is_post_revision( $post ) || wp_is_post_autosave( $post ) ) 
		return;

	$enabled_blogs = (array) get_site_option( 'ucc_mfp_blogs' );
	if ( in_array( $blog_id, $enabled_blogs ) ) {	
		$helper = new UCC_MFP_Helper;
		$featured = $helper->is_featured_post( $blog_id, $post );
		if ( $featured ) {
			// Try UPDATE first, since we edit more often than create a new post.
			$result = $helper->update_multisite_post( $site_id, $blog_id, $post, $postarr );
			if ( ! $result ) // Then try INSERT.
				$result = $helper->insert_multisite_post( $site_id, $blog_id, $post, $postarr );
		} else {
			// DELETE the row from our table.
			$result = $helper->delete_multisite_post( $site_id, $blog_id, $post ); 
		}
	}
}
add_action( 'wp_insert_post', 'ucc_mfp_wp_insert_post', 10, 2 );

function ucc_mfp_update_comment_count( $post_id, $new, $old ) {
	global $blog_id, $site_id, $wpdb;
	
	$new = (int) $new;
	
	$post_id = (int) $post_id;
	$blog_id = (int) $blog_id;
	$site_id = (int) $site_id;

	$multisite_posts_table = $wpdb->base_prefix . 'multisite_posts';
	$wpdb->update( $multisite_posts_table, array( 'comment_count' => $new ), array( 'post_id' => $post_id, 'site_id' => $site_id, 'blog_id' => $blog_id ) );
}
add_action('wp_update_comment_count', 'ucc_mfp_update_comment_count', 10, 3 );


// Load template tags, widget, shortcode.
include_once( plugin_dir_path( __FILE__ ) . 'template.php' );

?>

<?php

function get_featured_posts() {
	global $blog_id, $site_id, $wpdb;
	$blog_id = absint( $blog_id );
	$site_id = absint( $site_id );

	$key = 'ucc_mfp_' . $site_id . '_' . $blog_id;
	$group = 'ucc_mfp';
	$ttl = 60 * 60;
	if ( false === ( $ordered_featured_posts = wp_cache_get( $key, $group ) ) ) {

		$multisite_posts_table = $wpdb->base_prefix . 'multisite_posts';
		$multisite_postmeta_table = $wpdb->base_prefix . 'multisite_postmeta';
		$multisite_featured_posts_table = $wpdb->base_prefix . 'multisite_featured_posts';
	
		$sql = "SELECT $multisite_posts_table.id, $multisite_posts_table.site_id, $multisite_posts_table.blog_id, $multisite_posts_table.post_id 
			FROM $multisite_featured_posts_table, $multisite_posts_table, $multisite_postmeta_table
			WHERE 
				$multisite_featured_posts_table.site_id = $site_id AND
				$multisite_featured_posts_table.blog_id = $blog_id AND
				$multisite_featured_posts_table.featured_site_id = $multisite_posts_table.site_id AND
				$multisite_featured_posts_table.featured_blog_id = $multisite_posts_table.blog_id AND
				$multisite_featured_posts_table.featured_post_id = $multisite_posts_table.post_id AND
				$multisite_posts_table.post_password = '' AND
				$multisite_posts_table.post_date < NOW() AND
				$multisite_featured_posts_table.featured_site_id = $multisite_postmeta_table.site_id AND
				$multisite_featured_posts_table.featured_blog_id = $multisite_postmeta_table.blog_id AND
				$multisite_featured_posts_table.featured_post_id = $multisite_postmeta_table.post_id AND
				$multisite_postmeta_table.meta_key = 'ucc_mfp_featured_post'
			";
		$result = $wpdb->get_results( $sql, ARRAY_A );
		if ( $result )
			$total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );	
	
		$featured_posts = array();
		foreach ( $result as $row ) {
			$site = $row['site_id'];
			$blog = $row['blog_id'];
			$post = $row['post_id'];
		
			// Push posts to array to minimize future switch_to_blog and WP_Queries.
			$tmp = $featured_posts[$blog];
			if ( empty( $tmp ) )
				$tmp = array();
			
			$tmp[] = $post;
			$featured_posts[$blog] = $tmp;
		}

		$ordered_featured_posts = array();
		$blogs = array_keys( $featured_posts );
		if ( ! empty( $blogs ) && in_array( $blog_id, $blogs ) ) {
			$t_blog = $blog_id;
			$t_posts = $featured_posts[$blog_id];
			$ordered_featured_posts[] = array( $t_blog => $t_posts );
			unset( $featured_posts[$blog_id] );
		}
		foreach ( $featured_posts as $blog => $posts ) {
			$t_array = array( $blog => $posts );
			$ordered_featured_posts[] = $t_array;
		}
	
		wp_cache_set( $key, $ordered_featured_posts, $group, $ttl );
	}
	
	return $ordered_featured_posts;
}

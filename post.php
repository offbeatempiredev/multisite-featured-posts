<?php

if ( ! is_multisite() )
	wp_die( __( 'Multisite support is not enabled.' ) );

if ( ! current_user_can( 'manage_options' ) )
	wp_die( __( 'You do not have permission to access this page.' ) );

if ( ! empty( $_POST ) && check_admin_referer( basename( __FILE__ ), 'ucc_mfp_post_nonce' ) ) {
	// Require custom Helper class.
	require_once( plugin_dir_path( __FILE__ ) . 'library/class-ucc-mfp-helper.php' );
	$helper = new UCC_MFP_Helper;
	
	// Process actual data here.
	global $blog_id, $site_id, $wpdb;
	$blog_id = absint( $blog_id );
	$site_id = absint( $site_id );
	$multisite_featured_posts_table = $wpdb->base_prefix . 'multisite_featured_posts';
	
	if ( ( 'select' == $_REQUEST['mfpmode'] ) && isset( $_POST['ucc_mfp_shadow_select_posts'] ) ) {
		// Select Posts to Feature: checked means add
		$_select = $_POST['ucc_mfp_select_posts'];
		if ( empty( $_select ) )
			$_select = array();
		
		$_shadow_select = $_POST['ucc_mfp_shadow_select_posts'];
		if ( empty( $_shadow_select ) )
			$_shadow_select = array();

		foreach ( $_shadow_select as $_blog => $_posts ) {				
			foreach ( $_posts as $_post => $_checked ) {
				if ( 'checked' == $_select[$_blog][$_post] ) {
					$data = array(
						'site_id'          => $site_id,
						'blog_id'          => $blog_id,
						'featured_site_id' => $site_id,
						'featured_blog_id' => $_blog,
						'featured_post_id' => $_post 
					);
					$format = array( '%d', '%d', '%d', '%d', '%d' );
					$wpdb->insert( $multisite_featured_posts_table, $data, $format );
				} else {
					$sql = "DELETE FROM $multisite_featured_posts_table WHERE site_id = $site_id 
						AND blog_id = $blog_id 
						AND featured_site_id = $site_id 
						AND featured_blog_id = $_blog 
						AND featured_post_id = $_post";
					$result = $wpdb->query( $sql );
				}
			}
		}
		
		// Reset featured post cache for this blog.
		$key = 'ucc_mfp_' . $site_id . '_' . $blog_id;
		$group = 'ucc_mfp';
		wp_cache_delete( $key, $group );
	} elseif ( ( 'share' == $_REQUEST['mfpmode'] ) && isset( $_POST['ucc_mfp_share_posts'] ) ) {
		// Posts I'm Sharing: checked means remove
		$_share = $_POST['ucc_mfp_share_posts'];
		if ( empty( $_share ) )
			$_share = array();
			
		foreach ( $_share as $_blog => $_posts ) {
			foreach ( $_posts as $_post => $_checked ) {
				if ( 'checked' == $_checked ) {
					$helper->delete_multisite_post( $site_id, $blog_id, $_post );	
					delete_post_meta( $_post, 'ucc_mfp_featured_post' );
				}
			}
		}
	} elseif ( isset( $_POST['ucc_mfp_shadow_feature_posts'] ) ) {
		// Posts I'm Featuring: checked means remove
		$blogs = get_site_option( 'ucc_mfp_blogs' );
		if ( empty( $blogs ) )
			$blogs = array();

		$_feature = $_POST['ucc_mfp_feature_posts'];
		if ( empty( $_select ) )
			$_select = array();

		foreach ( $_feature as $_blog => $_posts ) {
			foreach ( $_posts as $_post => $_checked ) {
				if ( 'checked' == $_checked ) {
					$sql = "DELETE FROM $multisite_featured_posts_table WHERE site_id = $site_id 
						AND blog_id = $blog_id 
						AND featured_site_id = $site_id 
						AND featured_blog_id = $_blog 
						AND featured_post_id = $_post";
					$result = $wpdb->query( $sql );
				}
			}
		}
		
		// Reset featured post cache for this blog.
		$key = 'ucc_mfp_' . $site_id . '_' . $blog_id;
		$group = 'ucc_mfp';
		wp_cache_delete( $key, $group );
	}
}

// Require custom WP_List_Table class.
require_once( plugin_dir_path( __FILE__ ) . 'library/class-ucc-mfp-post.php' );
        
$wp_list_table = new UCC_MFP_Post_List_Table();
$wp_list_table->prepare_items();

?>
<div class="wrap">
<?php screen_icon(); ?>
<?php 

$mfpmode = $wp_list_table->mfpmode;
$select_attr = $feature_attr = $share_attr = '';
switch ( $mfpmode ) {
	case 'select':
		$select_attr = ' class="current"';
		break;
	case 'share':
		$share_attr = ' class="current"';
		break;
	default: // case 'feature'
		$feature_attr = ' class="current"';
}

?>
<h2><?php esc_html_e( 'Featured Posts' ) ?></h2>

<ul class='subsubsub'>
	<li class='feature'><a href='edit.php?page=ucc_mfp_post&mfpmode=feature'<?php echo $feature_attr ?>>Posts I'm Featuring <span class="count">(<?php echo $wp_list_table->feature_count ?>)</span></a> | </li>
	<li class='select'><a href='edit.php?page=ucc_mfp_post&mfpmode=select'<?php echo $select_attr ?>>Select Posts to Feature <span class="count">(<?php echo $wp_list_table->select_count ?>)</span></a> | </li>
	<li class='share'><a href='edit.php?page=ucc_mfp_post&mfpmode=share'<?php echo $share_attr ?>>Posts I'm Sharing <span class="count">(<?php echo $wp_list_table->share_count ?>)</span></a></li>
</ul>

<form id="ucc-mfp-post-list" action="" method="post">
<?php wp_nonce_field( basename( __FILE__ ), 'ucc_mfp_post_nonce' ); ?>
<?php $wp_list_table->display(); ?>
</form>
</div>
<?php

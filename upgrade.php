<?php

// Add featured post table on network plugin activation. UNION is not a desirable MySQL solution.
function ucc_mfp_activation() {
	global $wpdb;
	// We're not using dbDelta because it fails silently.
	
/*
	if ( $wpdb->get_var( $wpdb->query( "SHOW TABLES LIKE '" . $table_name . "'" ) ) ) {
		// Print a "using existing table" message. 
	} else {
		// Print a "creating new table" message.
	}
*/

        $charset_collate = '';
        if ( ! empty($wpdb->charset) )
                $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
        if ( ! empty($wpdb->collate) )
                $charset_collate .= " COLLATE $wpdb->collate";

	// Based on the $wpdb->post schema in wp-admin/includes/schema.php.
	$table_name = $wpdb->base_prefix . 'multisite_posts';
	$sql = "CREATE TABLE IF NOT EXISTS $table_name  (  
  		id bigint(20) unsigned NOT NULL auto_increment,
		site_id int(11) NOT NULL default '0',
		blog_id int(11) NOT NULL default '0',
  		post_id bigint(20) unsigned NOT NULL default '0',
		permalink text NOT NULL,
 		post_author bigint(20) unsigned NOT NULL default '0',
 		post_date datetime NOT NULL default '0000-00-00 00:00:00',
 		post_date_gmt datetime NOT NULL default '0000-00-00 00:00:00',
		post_content longtext NOT NULL,
 		post_title text NOT NULL,
		post_excerpt text NOT NULL,
 		post_status varchar(20) NOT NULL default 'publish',
		comment_status varchar(20) NOT NULL default 'open',
		ping_status varchar(20) NOT NULL default 'open',
		post_password varchar(20) NOT NULL default '',
 		post_name varchar(200) NOT NULL default '',
		to_ping text NOT NULL,
		pinged text NOT NULL,
  		post_modified datetime NOT NULL default '0000-00-00 00:00:00',
  		post_modified_gmt datetime NOT NULL default '0000-00-00 00:00:00',
		post_content_filtered text NOT NULL,
		post_parent bigint(20) unsigned NOT NULL default '0',
		guid varchar(255) NOT NULL default '',
		menu_order int(11) NOT NULL default '0',
  		post_type varchar(20) NOT NULL default 'post',
		post_mime_type varchar(100) NOT NULL default '',
  		comment_count bigint(20) NOT NULL default '0',
  		PRIMARY KEY  ID ( ID ),
		UNIQUE KEY  blog_post ( blog_ID, post_ID ),
  		KEY  post_name ( post_name ),
  		KEY  type_status_date ( post_type, post_status, post_date, post_ID, blog_ID ),
		KEY post_parent (post_parent),
  		KEY  post_author ( post_author ),  
		KEY  comment_count ( comment_count )
	) $charset_collate;";
	$results = $wpdb->query( $sql );

	// Based on the $wpdb->postmeta schema in wp-admin/includes/schema.php.
	$table_name = $wpdb->base_prefix . 'multisite_postmeta';
	$sql = "CREATE TABLE IF NOT EXISTS $table_name  (
		id bigint(20) unsigned NOT NULL auto_increment,
		site_id int(11) NOT NULL default '0',
		blog_id int(11) NOT NULL default '0', 
		meta_id bigint(20) unsigned NOT NULL default '0',
		post_id bigint(20) unsigned NOT NULL default '0',
		meta_key varchar(255) default NULL,
		meta_value longtext,
		PRIMARY KEY  id (id),
		UNIQUE KEY  site_blog_post_meta ( site_id, blog_id, post_id, meta_id ),
		KEY post_id (post_id),
		KEY meta_key (meta_key)
	) $charset_collate;";
	$results = $wpdb->query( $sql );
	
	// Based on the $wpdb->postmeta schema in wp-admin/includes/schema.php.
	$table_name = $wpdb->base_prefix . 'multisite_featured_posts';
	$sql = "CREATE TABLE IF NOT EXISTS $table_name  (
		id bigint(20) unsigned NOT NULL auto_increment,
		site_id int(11) NOT NULL default '0',
		blog_id int(11) NOT NULL default '0', 
		featured_site_id int(11) NOT NULL default '0',
		featured_blog_id int(11) NOT NULL default '0', 
		featured_post_id bigint(20) unsigned NOT NULL default '0',
		PRIMARY KEY  id (id),
		UNIQUE KEY  featured ( site_id, blog_id, featured_site_id, featured_blog_id, featured_post_id )	
	) $charset_collate;";
	$results = $wpdb->query( $sql );
	
	
/*

	// Based on the $wpdb->terms schema in wp-admin/includes/schema.php.
	$table_name = $wpdb->base_prefix . 'multisite_terms';
	$sql = "CREATE TABLE IF NOT EXISTS $table_name  (
		term_id bigint(20) unsigned NOT NULL,
		name varchar(200) NOT NULL default '',
		slug varchar(200) NOT NULL default '',
		term_group bigint(10) NOT NULL default 0,
		PRIMARY KEY  term_id ( term_id) ,
		UNIQUE KEY slug ( slug ),
		KEY name ( name )
	) $charset_collate;";
	$results = $wpdb->query( $sql );

	// Based on the $wpdb->term_relationships schema in wp-admin/includes/schema.php.
	$table_name = $wpdb->base_prefix . 'multisite_term_relationships';
	$sql = "CREATE TABLE IF NOT EXISTS $table_name  (
		id bigint(20) unsigned NOT NULL auto_increment,
		blog_id int(11) NOT NULL default '0',
		object_id bigint(20) unsigned NOT NULL default 0,
		term_taxonomy_id bigint(20) unsigned NOT NULL default 0,
		term_order int(11) NOT NULL default 0,
		PRIMARY KEY  (object_id,term_taxonomy_id),
		KEY term_taxonomy_id (term_taxonomy_id)
	) $charset_collate;
	$results = $wpdb->query( $sql );
*/
	// We're only dealing with post_tags at this point, so no need for a term_taxonomy table.
	update_site_option( 'ucc_mfp_db_version', '1.1' );
}
